<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="js">
<head>
    <meta charset="utf-8">
    <meta name="apps" content="{{ site_whitelabel('apps') }}">
    <meta name="author" content="{{ site_whitelabel('author') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="site-token" content="{{ site_token() }}">

    <meta property="og:type" content="website" />
    <meta property="og:title" content="ICO Elf coin | Elf-coin" />
    <meta property="og:description" content="Бодит болон дижитал хосолсон HYBRID ертөнцөөр холбогдсон ХҮЧИРХЭГЖСЭН хүмүүсээр дүүрэн ертөнц. Хүчирхэгжсэн хүн гэдэг нь өөрийн гэсэн зорилготой, аз жаргалтай, эрүүл, бусдыг хүчирхэгжүүлдэг, үнэ цэн түгээдэг хүнийг хэлнэ. WHITEPAPER ТАТАХ ICO НЭЭГДЭХЭД Day(s) : Hour(s) : Minute(s) : Second(s) ICO-Д ОРОЛЦОХ ЯДУУРЛЫГ БОЛОВСРОЛЫН АРГААР УСТГАНА! Дэлхийн хамгийн чухал асуудлуудыг бид технологи дээр суурилсан нийгэмд ээлтэй" />
    <meta property="og:url" content="https://ico.elf.mn/" />
    <meta property="og:site_name" content="ICO Elf coin" />
    <meta property="article:publisher" content="https://www.facebook.com/elf.verse/" />
    <meta property="og:image" content="http://elf.mn/wp-content/uploads/2021/11/Screen-Shot-2021-11-23-at-19.26.43-1024x550.png" />
    <meta property="og:image:width" content="1024" />
    <meta property="og:image:height" content="550" />

    <link rel="shortcut icon" href="{{ site_favicon() }}">
    <title>@yield('title') | {{ site_whitelabel('title') }}</title>
    <link rel="stylesheet" href="{{ asset(style_theme('vendor')) }}">
    <link rel="stylesheet" href="{{ asset(style_theme('user')) }}">
    @if( recaptcha() )
    <script src="https://www.google.com/recaptcha/api.js?render={{ recaptcha('site') }}"></script>
    @endif
    @stack('header')
    @if(get_setting('site_header_code', false))
    {{ html_string(get_setting('site_header_code')) }}
    @endif
</head>
@php
$auth_layout = (gws('theme_auth_layout', 'default'));
$logo_light = ($auth_layout=='center-dark') ? 'logo-light' : 'logo';
$body_class = ($auth_layout=='center-dark'||$auth_layout=='center-light') ? ' page-ath-alt' : '';
$body_bgc   = ($auth_layout=='center-dark') ? ' bg-secondary' : '';
$wrap_class = ($auth_layout=='default') ? ' flex-row-reverse' : '';

$header_logo = '<div class="page-ath-header"><a href="'.url('/').'" class="page-ath-logo d-flex justify-content-center"><img class="page-ath-logo-img" src="'. site_whitelabel($logo_light) .'" srcset="'. site_whitelabel($logo_light.'2x') .'" alt="'. site_whitelabel('name') .'"></a></div>';
@endphp
<body class="page-ath theme-modern page-ath-modern{{ $body_class.$body_bgc }}">

    <div class="page-ath-wrap{{ $wrap_class }}">
        <div class="page-ath-content grad_panel">

            {!! $header_logo !!}
            @yield('content')

            <div class="page-ath-footer front_anim">
                @if(is_show_social('login'))
                    {!! UserPanel::social_links('', ['class' => 'mb-3']) !!}
                    {!! UserPanel::footer_links(['lang' => true], ['class' => 'guttar-20px align-items-center']) !!}
                    {!! UserPanel::copyrights('div') !!}
                @else
                    {!! UserPanel::footer_links(['lang' => true, 'copyright'=>true], ['class' => 'guttar-20px align-items-center']) !!}
                @endif
            </div>
        </div>
        @if ($auth_layout=='default' || $auth_layout=='alter')
        <div class="page-ath-gfx" style="background-image: url({{ asset('images/ath-gfx.png') }});">
            <div class="w-100 d-flex justify-content-center">
                <div class="col-md-12 col-xl-12">

                    <div class="auth_intro">
                        <h1 class="text-center">
                            <span class="text-white m-1">ТҮРҮҮЛЖ</span>
                            <span class="text-primary m-1">ЗАХИАЛ</span>
                        </h1>
                        <h1 class="text-center">
                            <span class="text-white m-1">ТҮРҮҮЛЖ</span>
                            <span class="text-primary m-1">ЭЗЭМШ</span>
                        </h1>
                        <img src="{{ asset('images/intro.png') }}" alt="" class="intro_img">

                        <div class="d-flex justify-content-start align-items-center mb-2">
                            <span class="text-primary mr-3"><i class="fa fa-play"></i></span>
                            <p class="text-white">Түрүүлж захиалгаа хийж шилжүүлгээ баталгаажуулсан нь хамгийн эхэнд
                                койноо эзэмших болно.</p>
                        </div>

                        <div class="d-flex justify-content-start align-items-center mb-2">
                            <span class="text-primary mr-3"><i class="fa fa-play"></i></span>

                            <p class="text-white">Сүүлд захиалга баталгаажуулсан нь хүссэн захиалгын тоогоороо авах
                                боломжгүй байх магадлалтайг анхаарна уу</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-2">
                            <span class="text-primary mr-3"><i class="fa fa-play"></i></span>
                            <p class="text-white">Тийм учир бүртгэлийн мэдээллээ баталгаажуулсны дараа шууд гүйлгээ хийж
                                захиалгаа баталгаажуулаарай.</p>

                        </div>

                        <div class="row mt-5">
                            <div class="col-lg-6">

                                <p class="d-flex flex-column">
                                <span class="text-white">Олон нийтэд санал болгох хэмжээ:</span>
                                <span class="text-primary">150.000.000.000 ширхэг</span>
                                </p>


                                <p class="d-flex flex-column">
                                <span class="text-white">Захиалгын дээд хэмжээ:</span>
                                <span class="text-primary">Нэг хүний 15 сая төгрөг хүртэл</span>
                                </p>

                            </div>
                            <div class="col-lg-6">

                                <p class="d-flex flex-column">
                                    <span class="text-white">Нэгж койны анхны үнэ:</span>
                                    <span class="text-primary">0.30 төгрөг</span>
                                </p>

                                <p class="d-flex flex-column">
                                    <span class="text-white">Үргэлжлэх хугацаа:</span>
                                    <span class="text-primary">Эхлэх: 11 сарын 25-ны 11:00 цагаас</span>
                                    <span class="text-primary">Дуусах: 11 сарын 30-ны 23:00 цаг хүртэл</span>
                                </p>

                            </div>
                        </div>



                        <p class="text-white mt-5 text-center">Элф ертөнцөд тавтай морил<br/>
                            Танд амжилт хүсье!</p>

                    </div>


                </div>
            </div>
        </div>
        @endif
    </div>

@if(gws('theme_custom'))
    <link rel="stylesheet" href="{{ asset(style_theme('custom')) }}">
@endif
    <script>
        var base_url = "{{ url('/') }}",
        csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
        layouts_style = "modern";
    </script>
    <script src="{{ asset('assets/js/jquery.bundle.js').css_js_ver() }}"></script>
    <script src="{{ asset('assets/js/script.js').css_js_ver() }}"></script>
    <script type="text/javascript">
        jQuery(function(){
            var $frv = jQuery('.validate');
            if($frv.length > 0){ $frv.validate({ errorClass: "input-bordered-error error" }); }
        });
    </script>
    @stack('footer')

    @if(get_setting('site_footer_code', false))
    {{ html_string(get_setting('site_footer_code')) }}
    @endif
</body>
</html>
