@extends('layouts.user')
@section('title', __('KYC Verification'))
@php
    $has_sidebar = false;

    $kyc_title = ($user_kyc !== NULL && isset($_GET['thank_you'])) ? __('Begin your ID-Verification') : __('KYC Verification');
    $kyc_desc = ($user_kyc !== NULL && isset($_GET['thank_you'])) ? __('Verify your identity to participate in token sale.') : __('To comply with regulations each participant is required to go through identity verification (KYC/AML) to prevent fraud, money laundering operations, transactions banned under the sanctions regime or those which fund terrorism. Please, complete our fast and secure verification process to participate in token offerings.');
@endphp

@section('content')

    <h4>Redirecting ...</h4>

    <form action="{!! $formData['url'] !!}" method="GET" id="oath_dan_form">
        @foreach($formData['params'] as $key => $val)
            <div class="form-group">
                <input type="hidden" name="{!! $key !!}" value="{!! $val !!}"/>
            </div>
        @endforeach
    </form>

@endsection

@push('footer')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#oath_dan_form").submit();
        });
    </script>
@endpush
