@extends('layouts.user')
@section('title', __('KYC Verification'))
@php
$has_sidebar = false;

$kyc_title = ($user_kyc !== NULL && isset($_GET['thank_you'])) ? __('Begin your ID-Verification') : __('KYC Verification');
$kyc_desc = ($user_kyc !== NULL && isset($_GET['thank_you'])) ? __('Verify your identity to participate in token sale.') : __('To comply with regulations each participant is required to go through identity verification (KYC/AML) to prevent fraud, money laundering operations, transactions banned under the sanctions regime or those which fund terrorism. Please, complete our fast and secure verification process to participate in token offerings.');
@endphp

@section('content')
<div class="page-header page-header-kyc">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-xl-7 text-center">
            <h2 class="page-title">{{ $kyc_title }}</h2>
        </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-lg-10 col-xl-9">

        <div class="content-area card user-account-pages page-kyc">
            <div class="card-innr">
                <div class="kyc-status card mx-lg-4">
                    <div class="card-innr">

                        @if(!is_null($result['kyc']))

                            @php
                                $kyc = $result['kyc'];
                            @endphp

                            <div class="alert alert-success">
                                Таны мэдээлэл амжилттай баталгаажлаа
                            </div>

                            <ul class="data-details-list">
                                <li>
                                    <div class="data-details-head">{!! __('Firstname') !!}</div>
                                    <div class="data-details-des">{!! ($kyc->firstName) ? _x($kyc->firstName) : $space !!}</div>
                                </li>{{-- li --}}
                                <li>
                                    <div class="data-details-head">{!! __('Lastname') !!}</div>
                                    <div class="data-details-des">{!! ($kyc->lastName) ? _x($kyc->lastName) : $space !!}</div>
                                </li>{{-- li --}}
                                <li>
                                    <div class="data-details-head">{!! __('Register number') !!}</div>
                                    <div class="data-details-des">
                                        @if ($kyc->telegram)
                                            <span>{!! $kyc->telegram !!}</span>
                                        @endif
                                    </div>
                                </li>{{-- li --}}
                            </ul>

                        @else
                            <div class="alert alert-danger">
                                ДАН системээр баталгаажуулах үйлдэл амжилтгүй боллоо!
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>

        {!! UserPanel::kyc_footer_info() !!}
    </div>
</div>
@endsection
