@extends('layouts.user')
@section('title', __('Хөрөнгө оруулагч'))

@section('content')

    @include('layouts.messages')
    <div class="content-area card">
        <div class="card-innr" style="padding: 20px 60px;">



{{--            <div class="card-head" style="padding-top: 15px">--}}
{{--                <h4 class="card-title" style="text-align: center;font-size: 1.7em;">ELFC койны стратегийн хөрөнгө оруулах багцууд</h4>--}}
{{--            </div>--}}
{{--            <div class="gaps-3x"></div>--}}
{{--            <table class="data-table">--}}
{{--                <thead>--}}
{{--                <tr class="data-item data-head">--}}
{{--                    <th class="data-col pack" style="color: black;">Багц</th>--}}
{{--                    <th class="data-col price" style="color: black;">Хөрөнгө оруулах үнэ</th>--}}
{{--                    <th class="data-col time" style="color: black;">Түгжих хугацаа</th>--}}
{{--                    <th class="data-col percentage" style="color: black;">Урамшууллын хувь</th>--}}
{{--                    <th class="data-col condition" style="color: black;">Суллах нөхцөл</th>--}}
{{--                    <th class="data-col release" style="color: black;">Бүрэн сулрах хугацаа</th>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--                <tbody>--}}
{{--                <tr class="data-item">--}}
{{--                    <td class="data-col pack">--}}
{{--                        1--}}
{{--                    </td>--}}
{{--                    <td class="data-col price">--}}
{{--                        30,000,001₮ - 50,000,000₮--}}
{{--                    </td>--}}
{{--                    <td class="data-col time">--}}
{{--                        7 хоног--}}
{{--                    </td>--}}
{{--                    <td class="data-col percentage">--}}
{{--                        1 хувь--}}
{{--                    </td>--}}
{{--                    <td class="data-col condition" style="width: 25%;">--}}
{{--                        Нийт дансны--}}
{{--                        33% сулрах--}}
{{--                    </td>--}}
{{--                    <td class="data-col release" style="width: 25%;">--}}
{{--                        21 хоног--}}
{{--                        (давтамж: 3--}}
{{--                        удаа)--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--                <tr class="data-item">--}}
{{--                    <td class="data-col pack">--}}
{{--                        2--}}
{{--                    </td>--}}
{{--                    <td class="data-col price">--}}
{{--                        50,000,001₮ - 250,000,000₮--}}
{{--                    </td>--}}
{{--                    <td class="data-col time">--}}
{{--                        14 хоног--}}
{{--                    </td>--}}
{{--                    <td class="data-col percentage">--}}
{{--                        2 хувь--}}
{{--                    </td>--}}
{{--                    <td class="data-col condition" style="width: 25%;">--}}
{{--                        Нийт дансны--}}
{{--                        20% хувь--}}
{{--                        сулрах--}}
{{--                    </td>--}}
{{--                    <td class="data-col release" style="width: 25%;">--}}
{{--                        35 хоног--}}
{{--                        (давтамж: 5--}}
{{--                        удаа)--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--                <tr class="data-item">--}}
{{--                    <td class="data-col pack">--}}
{{--                        3--}}
{{--                    </td>--}}
{{--                    <td class="data-col price">--}}
{{--                        250,000,001₮ - 500,000,000₮--}}
{{--                    </td>--}}
{{--                    <td class="data-col time">--}}
{{--                        28 хоног--}}
{{--                    </td>--}}
{{--                    <td class="data-col percentage">--}}
{{--                        3 хувь--}}
{{--                    </td>--}}
{{--                    <td class="data-col condition" style="width: 25%;">--}}
{{--                        Нийт дансны--}}
{{--                        10% хувь--}}
{{--                        сулрах--}}
{{--                    </td>--}}
{{--                    <td class="data-col release" style="width: 25%;">--}}
{{--                        70 хоног--}}
{{--                        (давтамж: 10--}}
{{--                        удаа)--}}
{{--                    </td>--}}
{{--                <tr class="data-item">--}}
{{--                    <td class="data-col pack">--}}
{{--                        4--}}
{{--                    </td>--}}
{{--                    <td class="data-col price">--}}
{{--                        1,000,000,000₮ - ба түүнээс дээш--}}
{{--                    </td>--}}
{{--                    <td class="data-col time">--}}
{{--                        Стратегийн түншлэл--}}
{{--                    </td>--}}
{{--                    <td colspan="2" class="data-col percentage">--}}
{{--                        Та стратегийн түншлэлийн хувилбарыг сонгохыг хүсвэл төслийн багтай холбогдоно уу. Утас: 77773111--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--                </tbody>--}}
{{--            </table>--}}
{{--            <div class="gaps-1x"></div>--}}

{{--            <p class="text-danger text-center">Багцууд түгжих хугацаатай болохыг анхаарна уу. <br/>--}}
{{--                ELFC койны хоёрдогч зах зээлд гарах хүртэлх хугацаанд таны худалдаж авсан койн түгжигдэх болохыг анхаарна уу.</p>--}}

        </div>
    </div>
@endsection
