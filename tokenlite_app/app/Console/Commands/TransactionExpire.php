<?php

namespace App\Console\Commands;

use App\Models\PaymentMethod;
use App\Models\Transaction;
use App\PayModule\Qpay\QpayModule;
use Illuminate\Console\Command;

class TransactionExpire extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire inactive transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Transactions");

        $transactions = Transaction::where('user', 3)
            ->where('stage', 2)
            ->whereIn('status', ['approved', 'pending'])
            ->whereNull('refund')
            ->sum('tokens');

        dump($transactions);

    }


}
