<?php

namespace App\Console\Commands;

use App\PayModule\Bank1\Bank1GW;
use Illuminate\Console\Command;

class BankKhanProcess extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bank:khan:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire inactive transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bankProcess = new Bank1GW();
        $bankProcess->runProcess();
    }

}
