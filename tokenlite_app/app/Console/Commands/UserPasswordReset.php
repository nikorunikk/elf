<?php

namespace App\Console\Commands;

use App\Models\PaymentMethod;
use App\Models\Transaction;
use App\Models\User;
use App\PayModule\Qpay\QpayModule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Console\Command;

class UserPasswordReset extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:password:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire inactive transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::find(1);

        $hashed = Hash::make("Bileg89");

        $user->password = $hashed;
        $user->save();

        dump($user->email);
        dump($hashed);


    }

}
