<?php
/**
 * CryptoCurrency Address Validation
 *
 * This class retrieve the address is valid or not.
 *
 * @package TokenLite
 * @author Softnio
 * @version 1.1
 */

namespace App\Helpers;

use Auth;
use App\Models\KYC;
use Illuminate\Http\Request;

class OAuthDan
{

    const OAUTH_SERVER = "https://sso.gov.mn";
    const SESSION_NAME = "oauth_dan_state";

    const CLIENT_ID = "4b3383ed24b7e863dcd94c6c-994756c1ba1939ce9eb7abb7a4cc4a30";
    const CLIENT_SECRET = "ZWQ4ZGUzNGQ2YmFkNzIxOGJhNGJkY2Y5OWM1NTcyMWY3NjQxMWMyMjJhYjZkMDI3MDJkMmFhMjQzOWU0OWFiYTg0ZmFhNDMyNzkwY2MyMjVjZjkzZGQ5ODBhNGM1ODU4ZGFlOTgyMmE3MTYxYTRlNTEyNTU5ODlhYTdhNGNiY2E=";

    public function getScopeInfo()
    {
        $info = [
            [
                'services' => [
                    'WS100101_getCitizenIDCardInfo',
                ],
                'wsdl' => 'https://xyp.gov.mn/citizen-1.3.0/ws?WSDL',
            ],
//            [
//                'services' => [
//                    0 => 'WS100201_getPropertyInfo',
//                ],
//                'wsdl' => 'https://xyp.gov.mn/property-1.3.0/ws?WSDL',
//                'params' => [
//                    'WS100201_getPropertyInfo' => [
//                        'propertyNumber' => 'value',
//                    ],
//                ],
//            ],
        ];

        return base64_encode(json_encode($info));
    }

    public function generateState()
    {
        return md5("tes_" . time());
    }

    public function getRequestFormData()
    {
        /*
response_type - Тогтмол "code" байна.
client_id - "OAuth" системээс илгээсэн "Client ID".
redirect_uri - "OAuth" системээс илгээсэн "Redirect URI".
scope - base64_encode(json_encode(service_structure)) утга.
state - Клиент системээс үүсгэгдсэн дахин давтагдашгүй утга. Хүсэлт бүрт өөр байна. (CSRF халдлагаас хамгаалахад хэрэглэгдэнэ.)
         */

        $state = $this->generateState();
        session()->put(self::SESSION_NAME, $state);

//        $redirectUri = "https://ico.elf.mn/user/kyc/stat/dan";
        $redirectUri = route('user.kyc.stat');

        $data = [
            'url' => self::OAUTH_SERVER . "/oauth2/authorize",
            'params' => [
                'response_type' => 'code',
                'client_id' => self::CLIENT_ID,
                'redirect_uri' => $redirectUri,
                'scope' => $this->getScopeInfo(),
                'state' => $state,
            ],
        ];

        return $data;
    }

    public function useAccessGrant(Request $request)
    {

        /*
array:2 [▼
  "GET" => array:3 [▼
    "code" => "ifjAHuj-pMJjIXwUwC2DtiQ0kvWWKaEvcog1BzbBIKfmNEUERXqQXHQXkvTsCG39JwTJ_7ARiJjDcihsSR-vm6kICFsxvpS2JFl1YZKk6RbDS9UeudFfDbkHtOFYqrXIMTYzNjk1ODUwMy40NTU5NDE0"
    "expires" => "60"
    "state" => "8a76bcba6626a3746b3df3bd8fc19b1d"
  ]
  "POST" => []
]
         */

        $code = $request->query->get('code');
        $expires = $request->query->getInt('expires');
        $state = $request->query->get('state');

        if (session()->has(self::SESSION_NAME)) {
            $statePending = session(self::SESSION_NAME);
        }

        $authInfo = $this->sendAuthRequest($code);
        if (!(is_array($authInfo) && isset($authInfo['access_token']))) {
            return null;
        }

        $accessToken = $authInfo['access_token'];
        $response = $this->getUserInfo($accessToken);

        $userInfo = null;

        if (is_array($response)) {
            foreach ($response as $section) {
                if (isset($section['services']) && isset($section['services']['WS100101_getCitizenIDCardInfo'])) {
                    $userInfo = $section['services']['WS100101_getCitizenIDCardInfo']['response'];
                }
            }
        }

        $kyc = null;

        if (is_array($userInfo)) {
            $kyc = $this->createKYC($userInfo);
        }

        return [
            'kyc' => $kyc,
            'userInfo' => $userInfo,
        ];
    }


    public function sendAuthRequest($code)
    {
        /*
grant_type - Тогтмол “authorization_code” байна.
code - “Access Grant Response” буцаалтаар ирсэн code параметрийн утга (grant code).
client_id - “OAuth” системээс илгээгдсэн “Client ID”. Дэлгэрэнгүй…
client_secret - “OAuth” системээс илгээгдсэн “Client Secret Key”.
redirect_uri - “OAuth” системээс илгээгдсэн “Redirect URI”.
         */

        $data = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'redirect_uri' => route('user.kyc.stat'),
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::OAUTH_SERVER . '/oauth2/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/x-www-form-urlencoded',
            ],
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }

    public function getUserInfo($accessToken)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::OAUTH_SERVER . '/oauth2/api/v1/service',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $accessToken,
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response, true);
    }

    public function createKYC($userInfo)
    {

        $user = Auth::user();

        $dob = (new \DateTime($userInfo['birthDateAsText']))->format("m/d/Y");
        $gender = ($userInfo['gender'] == 'Эрэгтэй' ? 'Male' : ($userInfo['gender'] == 'Эмэгтэй' ? 'Female' : null));
        $doc1 = $this->saveDoc($userInfo['image']);

        $kyc = new KYC();
        $kyc->userId = $user->id;
        $kyc->firstName = $userInfo['firstname'];
        $kyc->lastName = $userInfo['lastname'];
        $kyc->email = $user->email;
//        $kyc_submit->phone =
        $kyc->dob = $dob;
        $kyc->gender = $gender;
        $kyc->telegram = mb_strtoupper(strip_tags($userInfo['regnum']), "UTF-8");

        $kyc->country = 'Mongolia';
        $kyc->state = $userInfo['aimagCityName'];
        $kyc->city = $userInfo['aimagCityName'];
//        $kyc_submit->zip = strip_tags($request->input('zip'));
        $kyc->address1 = $userInfo['passportAddress'];
        $kyc->address2 = $userInfo['passportAddress'];

        $kyc->documentType = "oauth_dan";
        $kyc->document = $doc1;
//        $kyc_submit->document2 = $doc2;
//        $kyc_submit->document3 = $doc3;
        $kyc->status = 'approved';

//        $kyc_submit->walletName = strip_tags($request->input('wallet_name'));
//        $kyc_submit->walletAddress = strip_tags($request->input('wallet_address'));

        $kyc->save();

        return $kyc;
    }

    public function saveDoc($base64)
    {
        $fileContent = base64_decode($base64);
        $ds = DIRECTORY_SEPARATOR;
        $path = storage_path('') . $ds . "app" . $ds . "kyc-files" . $ds;
        $filename = (auth()->check() ? set_id(auth()->id()) : str_random(10)) . '-' . 'doc-one' . '-' . time() . '.' . 'png';
        file_put_contents($path . $filename, $fileContent);
        return "kyc-files/" . $filename;
    }

}
