<?php

namespace App\Http\Middleware;

use Closure;

class ApiFilterMiddleware
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = [
            'status' => 'error',
            'msg' => '',
            'data' => [],
        ];

        $curDate = new \DateTime('today');
        $hash = md5("ICO-ELFCOIN-" . $curDate->format('Y-m-d'));

        if ($hash != $request->bearerToken()) {

            $date = new \DateTime('now');

            $response['msg'] = 'Access denied!';
            $response['data'][] = $date->format('Y-m-d H:i:s');
            return response()->json($response);
        }

        return $next($request);
    }
}
