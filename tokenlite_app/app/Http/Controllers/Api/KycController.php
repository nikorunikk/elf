<?php

namespace App\Http\Controllers\Api;
/**
 * Kyc Controller
 *
 *
 * @package TokenLite
 * @author Softnio
 * @version 1.0.6
 */
use Auth;
use Validator;
use IcoHandler;
use App\Models\KYC;
use App\Models\User;
use App\Models\UserMeta;
use App\Helpers\ReCaptcha;
use Illuminate\Http\Request;
use App\Notifications\KycStatus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class KycController extends Controller
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function userList(Request $request, $user_id)
    {

        $user_id = (int) $user_id;
        $kycs = KYC::where('userId', $user_id)
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => [
                'user_id' => $user_id,
                'kycs' => $kycs,
            ],
        ]);
    }


}
