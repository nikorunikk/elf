<?php

namespace App\Http\Controllers\Api;
/**
 * Kyc Controller
 *
 *
 * @package TokenLite
 * @author Softnio
 * @version 1.0.6
 */

use Auth;
use Validator;
use IcoHandler;
use App\Models\KYC;
use App\Models\User;
use App\Models\UserMeta;
use App\Helpers\ReCaptcha;
use Illuminate\Http\Request;
use App\Notifications\KycStatus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;

class UserController extends Controller
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function list(Request $request, $minId, $limit)
    {
        $minId = (int) $minId;
        $limit = (int) $limit;

        $users = User::where('role', 'user')
            ->where('id', '>', $minId)
            ->take($limit)
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => [
                'min_id' => $minId,
                'limit' => $limit,
                'users' => $users,
            ],
        ]);
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function check(Request $request)
    {
        $data = $request->json()->all();

        $user = User::where('email', $data['email'])->first();

        $response = [
            'status' => 'error',
            'msg' => '',
            'data' => [],
        ];

        if (!$user) {
            $response['msg'] = 'User not found';
            return response()->json($response);
        }

        if (!Hash::check($data['password'], $user->password)) {
            $response['msg'] = 'Password wrong';
            return response()->json($response);
        }

        $response['status'] = 'success';
        $response['data'] = ['user' => $user];

        return response()->json($response);
    }

}
