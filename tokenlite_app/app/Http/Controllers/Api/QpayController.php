<?php

namespace App\Http\Controllers\Api;
/**
 * Kyc Controller
 *
 *
 * @package TokenLite
 * @author Softnio
 * @version 1.0.6
 */

use App\Models\Transaction;
use App\PayModule\Qpay\Qpay;
use Auth;
use Validator;
use IcoHandler;
use App\Models\KYC;
use App\Models\User;
use App\Models\UserMeta;
use Illuminate\Http\Request;
use App\Notifications\KycStatus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;

class QpayController extends Controller
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function check(Request $request, $tnxId)
    {

        $textResult = "";

        try {

            $tnx = Transaction::where('tnx_id', $tnxId)->first();

            if ($tnx) {
                $qpay = new Qpay();
                $qpay->checkTransaction($tnx);
                $textResult = "success";
            } else {
                $textResult = "not found";
            }
            $statusCode = 200;

        } catch (\Exception $exp) {
            $textResult = "error - " . $exp->getMessage();
            $statusCode = 500;
        }

        return response($textResult, $statusCode);
    }

}
