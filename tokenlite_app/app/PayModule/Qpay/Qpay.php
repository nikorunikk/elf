<?php

namespace App\PayModule\Qpay;

use App\Helpers\ReferralHelper;
use App\Models\IcoStage;
use App\Models\PaymentMethod;
use App\Models\Transaction;
use App\Notifications\TnxStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Qpay
{
//    const URL_BASE = "https://merchant-sandbox.qpay.mn/v2";
    const URL_BASE = "https://merchant.qpay.mn/v2";

    /** @var string */
    protected $username;

    /** @var string */
    protected $password;

    /** @var string */
    protected $invoiceCode;

    public function __construct()
    {
        $qpayData = PaymentMethod::get_data(QpayModule::SLUG);
        $this->username = $qpayData->username;
        $this->password = $qpayData->password;
        $this->invoiceCode = $qpayData->invoice_code;
    }

    function createToken()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::URL_BASE . '/auth/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_USERPWD => $this->username . ":" . $this->password,
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: Basic VEVTVF9NRVJDSEFOVDoxMjM0NTY=',
//            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response, true);
    }

    public function createInvoice(Transaction $transaction)
    {
        $url = self::URL_BASE . '/invoice';
        $request = array(
            'invoice_code' => $this->invoiceCode,
            'sender_invoice_no' => $transaction->tnx_id,
            'invoice_receiver_code' => 'terminal',
            'invoice_description' => 'ELFC token',
            'amount' => $transaction->amount,
            'callback_url' => route('_api.qpay.check', ['tnxId' => $transaction->tnx_id]),
        );

        $qpayRes = $this->curl($url, $request);

        $transaction->payment_id = $qpayRes['invoice_id'];
        $transaction->extra = json_encode($qpayRes);

        $transaction->save();

        return $qpayRes;
    }

    public function checkInvoiceGet($order_id)
    {
        $token = $this->createToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::URL_BASE . '/invoice/' . $order_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token['access_token'],
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response, true);
    }

    public function checkInvoice($order_id)
    {
        $url = self::URL_BASE . '/payment/check';
        $request = array(
            'object_type' => 'INVOICE',
            'object_id' => $order_id,
            'offset' =>
                array(
                    'page_number' => 1,
                    'page_limit' => 100,
                ),
        );
        return $this->curl($url, $request);
    }

    function curl($url, $request)
    {
        $token = $this->createToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($request),
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token['access_token'],
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);
    }


    /**
     * Success Callback Payment via PayPal
     *
     * @return void
     * @since 1.0.2
     * @version 1.0.0
     */
    public function checkTransaction(Transaction $tranx)
    {
        $response = [];
        $response['msg'] = 'error';
        $response['message'] = __('Something is wrong! Unable to process your request.');

        $payment = $this->checkInvoice($tranx->payment_id);

        if (!(is_array($payment) && count($payment['rows']) > 0)) {
            $response['msg'] = 'warning';
            $response['message'] = __('Your invoice is not paid :invoiceId', ['invoiceId' => $tranx->tnx_id]);
            return $response;
        }

        /*
[
  "payment_id" => "283894225628713"
  "payment_status" => "PAID"
  "payment_amount" => "200.00"
  "trx_fee" => "0.00"
  "payment_currency" => "MNT"
  "payment_wallet" => "Төрийн банк апп"
  "payment_type" => "P2P"
  "next_payment_date" => null
  "next_payment_datetime" => null
  "card_transactions" => []
  "p2p_transactions" => array:1 [
    0 => array:8 [
      "transaction_bank_code" => "340000"
      "account_bank_code" => "050000"
      "account_bank_name" => "Төрийн банк"
      "account_number" => "5926401325"
      "status" => "SUCCESS"
      "amount" => "200.00"
      "currency" => "MNT"
      "settlement_status" => "SETTLED"
    ]
  ]
]
         */

        $payment = $payment['rows'][0];
        $_old_status = $tranx->status;

        if ($payment['payment_status'] == 'PAID' && $_old_status == 'approved') {
            $response['msg'] = 'success';
            $response['message'] = __('Thank You, We have received your payment! Please check your token balance.');
            return $response;
        }

//        $tranx->status = ($payment->status == 'COMPLETED') ? 'approved' : (($payment->status == 'VOIDED') ? 'rejected' : 'canceled');
        $tranx->status = ($payment['payment_status'] == 'PAID') ? 'approved' : (($payment['payment_status'] == 'VOIDED') ? 'rejected' : 'canceled');

        if ($payment['payment_status'] == 'PAID') {


//            $tranx->wallet_address = data_get($payment, 'payer.email_address');
            $tranx->receive_amount = $payment['payment_amount'];
//            $tranx->tnx_time = date('Y-m-d H:i:s', strtotime(data_get($payment, 'purchase_units.0.payments.captures.0.create_time', 'now')));
            $tranx->payment_to = "ELFC , QPay";
            $tranx->checked_by = json_encode(['name' => 'qpay', 'id' => $tranx->payment_id]);
            $tranx->checked_time = Carbon::now()->toDateTimeString();
            $tranx->extra = json_encode((array)$payment);
            $tranx->save();

            if ($_old_status == 'deleted') {
                $tranx->status = 'missing';
                $tranx->save();

                $response['msg'] = 'warning';
                $response['message'] = 'Thank you! We received your payment but we found something wrong in your transaction, please contact with us with the order id: ' . $tranx->tnx_id . '.';

            } else {
                if ($tranx->status == 'approved' && is_active_referral_system()) {
                    $referral = new ReferralHelper($tranx);
                    $referral->addToken('refer_to');
                    $referral->addToken('refer_by');
                }
                IcoStage::token_add_to_account($tranx, null, 'add');
                try {
                    $tranx->tnxUser->notify((new TnxStatus($tranx, 'successful-user')));
                    if (get_emailt('order-successful-user', 'notify') == 1) {
                        notify_admin($tranx, 'order-successful-user');
                    }
                } catch (\Exception $e) {
                    $response['error'] = $e->getMessage();
                }

                $response['msg'] = 'success';
                $response['message'] = __('Thank You, We have received your payment! Please check your token balance.');
            }
        } else {
//            $tranx->save();
//            IcoStage::token_add_to_account($tranx, 'sub');

            $response['msg'] = 'warning';
            $response['message'] = sprintf('Your invoice is not paid (%s)', $tranx->tnx_id);
        }

        return $response;
    }

    public function payment_cancel(Request $request, $name = 'Order has been canceled due to payment!')
    {
        if ($request->get('tnx_id') || $request->get('token')) {
            $id = $request->get('tnx_id');
            $pay_token = $request->get('token');
            if ($pay_token != null) {
                $pay_token = (starts_with($pay_token, 'EC-') ? str_replace('EC-', '', $pay_token) : $pay_token);
            }
            $apv_name = ucfirst('paypal');
            if (!empty($id)) {
                $tnx = Transaction::where('id', $id)->first();
            } elseif (!empty($pay_token)) {
                $tnx = Transaction::where('payment_id', $pay_token)->first();
                if (empty($tnx)) {
                    $tnx = Transaction::where('extra', 'like', '%' . $pay_token . '%')->first();
                }
            } else {
                return redirect(route('user.token'))->with(['danger' => "Sorry, we're unable to proceed the transaction. This transaction may deleted. Please contact with administrator.", 'modal' => 'danger']);
            }
            if ($tnx) {
                $_old_status = $tnx->status;
                if ($_old_status == 'deleted' || $_old_status == 'canceled') {
                    $name = "Your transaction is already " . $_old_status . ". Sorry, we're unable to proceed the transaction.";
                } elseif ($_old_status == 'approved') {
                    $name = "Your transaction is already " . $_old_status . ". Please check your account balance.";
                } elseif (!empty($tnx) && ($tnx->status == 'pending' || $tnx->status == 'onhold')) {
                    $tnx->status = 'canceled';
                    $tnx->checked_by = json_encode(['name' => $apv_name, 'id' => $pay_token]);
                    $tnx->checked_time = Carbon::now()->toDateTimeString();
                    $tnx->save();

                    IcoStage::token_add_to_account($tnx, 'sub');
                    try {
                        $tnx->tnxUser->notify((new TnxStatus($tnx, 'canceled-user')));
                        if (get_emailt('order-rejected-admin', 'notify') == 1) {
                            notify_admin($tnx, 'rejected-admin');
                        }
                    } catch (\Exception $e) {
                        $response['error'] = $e->getMessage();
                    }
                }
            } else {
                $name = "Transaction is not found!!";
            }
        } else {
            $name = "Transaction id or key is not valid!";
        }
        return redirect(route('user.token'))->with(['danger' => $name, 'modal' => 'danger']);
    }

}
