@php
    $bank = get_pm($transaction->payment_method);
@endphp
<a href="#" class="modal-close" data-dismiss="modal"><em class="ti ti-close"></em></a>
<div class="popup-body">
    <h4 class="popup-title">{{__('Pay by QPay')}}</h4>
    <div class="popup-content">

        <p class="lead-lg text-primary">{!! __('Your Order no. :orderid has been placed successfully.', ['orderid' => '<strong>'.$transaction->tnx_id.'</strong>' ]) !!}</p>

        <p>
            {!! __('Please pay your :amount :currency by scanning QR code below.',
    [
    'amount' => '<strong class="text-primary">'.to_num($transaction->amount, 'max').'</strong>',
    'currency' => '₮',

    ]) !!}
        </p>

        <div class="gaps-1x"></div>
        <form action="{{ route('payment.qpay.update') }}" method="POST" id="payment-confirm" class="validate-modern"
              autocomplete="off">
            @csrf
            <input type="hidden" name="trnx_id" value="{{ $transaction->id }}">

            <p>Хэрвээ та банкны апп нээх бол <a href="{{ $qpayRes['qPay_shortUrl'] }}" class="text-primary font-weight-bold" target="_blank">ЭНД</a> дарна уу!</p>

            <h5 class="text-head mgb-0-5x"><strong>{{__('QR code')}}</strong></h5>

            <div class="d-flex justify-content-center align-items-center">
                <img src="data:image/png;base64, {{ $qpayRes['qr_image'] }}" alt="Red dot" style="max-width: 100%;"/>
            </div>


            <div class="gaps-0-5x"></div>
            <ul class="d-flex flex-wrap align-items-center guttar-30px">
                <li><a class="btn btn-auto btn-sm btn-primary"
                       href="{{ route('user.transactions') }}">{{__('View Transaction')}}</a>
                </li>
                <li class="pdt-1x pdb-1x">
                    <button type="submit" name="action" value="check"
                            class="btn btn-auto btn-sm btn-primary">{{__('Check Order')}}</button>
                </li>
                <li class="pdt-1x pdb-1x">
                    <button type="submit" name="action" value="cancel"
                            class="btn btn-cancel btn-danger-alt payment-cancel-btn payment-btn btn-simple">{{__('Cancel Order')}}</button>
                </li>
            </ul>

            <div class="gaps-2-5x"></div>
            <div class="note note-info note-plane">
                <em class="fas fa-info-circle"></em>
                <p>{!! __('Warning: After completing your QPay transaction please click on :checkbutton button.', ['checkbutton' => '<strong class="text-uppercase">' . __('Check Order') . '</strong>']) !!}</p>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    (function ($) {
        var $_p_form = $('form#payment-confirm'), _url = "{{ route('payment.qpay.notify') }}",
            _data = {
                tnx: {{ $transaction->id }},
                notify: "{{ $mailed['notify'] }}",
                user: "{{ $mailed['user'] }}",
                system: "{{ $mailed['system'] }}"
            };
        if (_url && _data) {
            ajax_email(_url, _data);
        }
        if ($_p_form.length > 0) {
            purchase_form_submit($_p_form);
        }
        $('.close-modal, .modal-close').on('click', function (e) {
            e.preventDefault();
            var $link = $(this).attr('href');
            $(this).parents('.modal').modal('hide');
            window.location.reload();
        });
    })(jQuery);
</script>
