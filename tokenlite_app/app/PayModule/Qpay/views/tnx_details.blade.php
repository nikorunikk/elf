@php
$data = json_decode($transaction->extra);
$j = json_decode($transaction->checked_by);
$transaction_cur = $transaction->currency;
$bank = get_pm($transaction->payment_method);
$qpayRes = json_decode($transaction->extra, true);
@endphp
<div class="modal fade" id="transaction-details" tabindex="-1">
    <div class="modal-dialog modal-dialog-md modal-dialog-centered">
        <div class="modal-content">
            @if($transaction)
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            @endif

            <div class="popup-body">
            @if($transaction)
                @if($transaction->status=='pending' || $transaction->status == 'onhold')
                    <h4 class="popup-title">{{__('Confirmation Your Payment')}}</h4>
                    <div class="content-area popup-content">


                        <p class="lead-lg text-primary">{!! __('Your Order no. :orderid has been placed & waiting for payment.', ['orderid' => '<strong>'.$transaction->tnx_id.'</strong>' ]) !!}</p>


                        <p>
                            {!! __('Please pay your :amount :currency by scanning QR code below.',
                    [
                    'amount' => '<strong class="text-primary">'.to_num($transaction->amount, 'max').'</strong>',
                    'currency' => '₮',

                    ]) !!}
                        </p>


                        <div class="gaps-1x"></div>
                        <form action="{{ route('payment.qpay.update') }}" method="POST" id="payment-confirm" class="validate" autocomplete="off">
                            @csrf
                            <input type="hidden" name="trnx_id" value="{{ $transaction->id }}">
                            <h5 class="text-head mgb-0-5x"><strong>{{__('Bank Details for Payment')}}</strong></h5>

                            <p>Хэрвээ та банкны апп нээх бол <a href="{{ $qpayRes['qPay_shortUrl'] }}" class="text-primary font-weight-bold" target="_blank">ЭНД</a> дарна уу!</p>

                            <h5 class="text-head mgb-0-5x"><strong>{{__('QR code')}}</strong></h5>

                            <div class="d-flex justify-content-center align-items-center">
                                <img src="data:image/png;base64, {{ $qpayRes['qr_image'] }}" alt="Red dot" style="max-width: 100%;"/>
                            </div>

                            <button type="submit" name="action" value="check"
                                        class="btn btn-auto btn-sm btn-primary mb-4">{{__('Check Order')}}</button>


                            <button type="submit" name="action" value="cancel" class="btn btn-cancel btn-danger-alt payment-cancel-btn payment-btn">{{__('Cancel Order')}}</button>
                            <div class="gaps-2-5x"></div>
                            <div class="note note-info note-plane">
                                <em class="fas fa-info-circle"></em>

                                <p>{!! __('Warning: After completing your QPay transaction please click on :checkbutton button.', ['checkbutton' => '<strong class="text-uppercase">' . __('Check Order') . '</strong>']) !!}</p>

                            </div>
                        </form>
                    </div>
                @else
                    <div class="content-area popup-content">
                        @include('layouts.token-details', ['transaction' => $transaction, 'details' => true])
                    </div>
                @endif
            @else
                <div class="content-area text-center popup-content">
                    <div class="status status-error">
                        <em class="ti ti-alert"></em>
                    </div>
                    <h3>{{__('Oops!!!')}}</h3>
                    <p>{!! __('Sorry, seems there is an issues occurred and we couldn’t process your request. Please contact us with your order no. :orderid, if you continue to having the issues.', ['orderid' => '<strong>'.$transaction->tnx_id.'</strong>']) !!}</p>
                    <div class="gaps-2x"></div>
                    <a href="#" data-dismiss="modal" data-toggle="modal" class="btn btn-light-alt">{{__('Close')}}</a>
                    <div class="gaps-3x"></div>
                </div>
            @endif

            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function($) {
            var $_p_form = $('form#payment-confirm');
            if ($_p_form.length > 0) {
                purchase_form_submit($_p_form);
            }
        })(jQuery);
    </script>
</div>
