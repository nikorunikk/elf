<div class="page-content">
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head d-flex justify-content-between align-items-center">
                    <h4 class="card-title mb-0">Bank Transfer Method</h4>
                    <a href="{{ route('admin.payments.setup') }}" class="btn btn-sm btn-auto btn-outline btn-primary d-sm-inline-block"><em class="fas fa-arrow-left"></em><span class="d-none d-sm-inline-block">Back</span></a>
                </div>
                <div class="gaps-1x"></div>
                <div class="card-text">
                    <p>Please enter your bank details for intentional or local bank transfer. All contributors received this details when they purchase your token in USD.</p>
                </div>
                <div class="gaps-2x"></div>
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('admin.ajax.payments.update') }}" method="POST" class="payment_methods_form validate-modern">
                            @csrf
                            <input type="hidden" name="req_type" value="qpay">
                            <div class="row align-items-center">
                                <div class="col-sm col-md-3">
                                    <label class="card-title card-title-sm">Active or Deactive</label>
                                </div>
                                <div class="col-sm col-md-3">
                                    <div class="fake-class">
                                        <div class="input-wrap input-wrap-switch">
                                            <input class="input-switch" {{ $pmData->status == 'active' ? 'checked' : '' }} id="status" name="status" type="checkbox">
                                            <label for="status">
                                                <span class="over">Inactive</span><span>Active Gateway</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gaps-1x"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">Method Title</label>
                                        <div class="input-wrap">
                                            <input class="input-bordered" value="{{ $pmData->title }}" type="text" name="title" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-item input-with-label">
                                        <label class="input-item-label">Description</label>
                                        <div class="input-wrap">
                                            <input class="input-bordered" value="{{ $pmData->details }}" placeholder="You can send paymeny direct to our wallets; We will manually verify" type="text" name="details">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h4>SAP</h4>
                                    <div class="sap"></div>
                                </div>
                            </div>
                            <div class="bank-details pt-3">
                                <div class="row">
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">Username</label>
                                            <div class="input-wrap">
                                                <input class="input-bordered required" name="username" value="{{ isset($pmData->secret->username) ? $pmData->secret->username : '' }}" type="text" placeholder="Enter Q-Pay merchant username" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">Password</label>
                                            <div class="input-wrap">
                                                <input class="input-bordered required" name="password" value="{{ isset($pmData->secret->password) ? $pmData->secret->password : '' }}" type="text" placeholder="Enter Q-Pay merchant password" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">Invoice code</label>
                                            <div class="input-wrap">
                                                <input class="input-bordered required" name="invoice_code" value="{{ isset($pmData->secret->invoice_code) ? $pmData->secret->invoice_code : '' }}" type="text" placeholder="Enter Q-Pay merchant invoice_code" required>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>{{-- .payment-wallet --}}
                            <div class="gaps-1x"></div>
                            <div class="d-flex pb-1">
                                <button class="btn btn-md btn-primary save-disabled" disabled type="submit">UPDATE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>{{-- .container --}}
</div>
