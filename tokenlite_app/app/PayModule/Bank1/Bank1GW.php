<?php

namespace App\PayModule\Bank1;

use App\Helpers\ReferralHelper;
use App\Models\IcoStage;
use App\Models\Transaction;
use App\Notifications\TnxStatus;
use Carbon\Carbon;

class Bank1GW
{
    const URL_BASE = "https://api.khanbank.com";

    /*
     * consumerKey: kGIKJgnkyyxU4kF6tZs9A6psvm0JYsBA	secretKey:8dEzyWCRD9Ukch8I
     */

    /** @var string */
    protected $customerKey;

    /** @var string */
    protected $secretKey;

    /** @var string */
    protected $accessToken;

    /** @var string */
    protected $account;

    public function __construct()
    {
        $this->customerKey = "kGIKJgnkyyxU4kF6tZs9A6psvm0JYsBA";
        $this->secretKey = "8dEzyWCRD9Ukch8I";
        $this->account = "5131435299";
    }


    function createToken()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::URL_BASE . '/v1/auth/token?grant_type=client_credentials',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_USERPWD => $this->customerKey . ":" . $this->secretKey,
            CURLOPT_POSTFIELDS => http_build_query([]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
//                'Authorization: Basic a0dJS0pnbmt5eXhVNGtGNnRaczlBNnBzdm0wSllzQkE6OGRFenlXQ1JEOVVrY2g4SQ=='
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response, true);

        if (is_array($response) && isset($response['access_token'])) {
            $this->accessToken = $response['access_token'];
            $res = true;
        } else {
            $this->accessToken = $response['access_token'];
            $res = false;
        }
        return $res;
    }

    protected function sendRequest()
    {
        if (!$this->createToken()) {
            return null;
        }

        $dateTo = new \DateTime('today');
        $dateFrom = clone $dateTo;
        $dateFrom->modify("-1 days");

        $url = self::URL_BASE . "/v1/statements/" . $this->account .
            "?from=" . $dateFrom->format('Ymd') .
            "&to=" . $dateTo->format("Ymd");

        $response = $this->sendGetRequest($url);
        $transactions = [];

        if (is_array($response)) {
            if (is_array($response['transactions'])) {
                $transactions = $response['transactions'];
            }
        }

        return $transactions;
    }

    public function runProcess()
    {
        $transactions = $this->sendRequest();
//        $transactions = $this->getSampleRequest();

        foreach ($transactions as $transaction) {
            $this->processTransactionInfo($transaction);
        }

    }

    protected function processTransactionInfo($tnxInfo)
    {
        /*
[
   'record' => 5,
   'tranDate' => '2021-11-21',
   'postDate' => '2021-11-21',
   'time' => '14123083',
   'branch' => '5000',
   'teller' => '99951',
   'journal' => 10204185,
   'code' => 1034,
   'amount' => 1000.0,
   'balance' => 21000.0,
   'debit' => 0.0,
   'correction' => 0,
   'description' => 'TEST GUILGEE ХААНААС: 150000 ЗОЛЗАЯА АРИЛД',
   'relatedAccount' => '2719323024',
]
        */

        $tnxId = $this->findTnxId($tnxInfo['description']);
        $tnx = Transaction::where('tnx_id', $tnxId)
            ->where('tnx_type', 'purchase')
            ->first();

        if (is_null($tnx)) {
            return;
        }

        if ($tnx->status == 'approved') {
            return;
        }

        if ($tnx->status == 'deleted') {
            $tnx->status = 'missing';
            $tnx->save();
            return;
        }

        if ($tnx->amount == $tnxInfo['amount']) {

            $tnxTime = new \DateTime('now');
            $tnxTime->setTimestamp($tnxInfo['time']);

            $tnx->status = 'approved';
            $tnx->payment_id = $tnxInfo['journal'];
            $tnx->receive_amount = $tnxInfo['amount'];
            $tnx->tnx_time = $tnxTime;
            $tnx->payment_to = "ELFC , Khanbank";
            $tnx->checked_by = json_encode(['name' => 'Khanbank Corporate GW', 'id' => $tnx->payment_id]);
            $tnx->checked_time = Carbon::now()->toDateTimeString();
            $tnx->extra = json_encode((array)$tnxInfo);
            $tnx->save();


            if (is_active_referral_system()) {
                $referral = new ReferralHelper($tnx);
                $referral->addToken('refer_to');
                $referral->addToken('refer_by');
            }
            IcoStage::token_add_to_account($tnx, null, 'add');
            try {
                $tnx->tnxUser->notify((new TnxStatus($tnx, 'successful-user')));
                if (get_emailt('order-successful-user', 'notify') == 1) {
                    notify_admin($tnx, 'order-successful-user');
                }
            } catch (\Exception $e) {
            }

        } else {
            echo "Amount: " . $tnx->amount . "<>" . $tnxInfo['amount'] . "\n";
        }


    }


    public function getSampleRequest()
    {

        $response = [
            'account' => '5131435299',
            'iban' => 'MN120005005131435299',
            'currency' => 'MNT',
            'customerName' => 'МЕТАВЕРС  ',
            'productName' => 'ХАРИЛЦАХ/БАЙГУУЛЛАГА / MNT',
            'branch' => '5131',
            'branchName' => 'ХУ ОРГИЛ ТТ',
            'beginBalance' => 20000.0,
            'endBalance' => 22500.0,
            'beginDate' => '2021-11-20',
            'endDate' => '2021-11-21',
            'total' => [
                'count' => 2,
                'credit' => 2500.0,
                'debit' => 0.0,
            ],
            'transactions' => [
                [
                    'record' => 5,
                    'tranDate' => '2021-11-21',
                    'postDate' => '2021-11-21',
                    'time' => '18202647',
                    'branch' => '5000',
                    'teller' => '99951',
                    'journal' => 10204185,
                    'code' => 1034,
                    'amount' => 4000.0,
                    'balance' => 21000.0,
                    'debit' => 0.0,
                    'correction' => 0,
                    'description' => 'TES000002 GUILGEE ХААНААС: 150000 ЗОЛЗАЯА АРИЛД',
                    'relatedAccount' => '2719323024',
                ],
                [
                    'record' => 9,
                    'tranDate' => '2021-11-21',
                    'postDate' => '2021-11-21',
                    'time' => '18202647',
                    'branch' => '5000',
                    'teller' => '99951',
                    'journal' => 18858197,
                    'code' => 1034,
                    'amount' => 4000.0,
                    'balance' => 22500.0,
                    'debit' => 0.0,
                    'correction' => 0,
                    'description' => 'TES000001 ХААНААС: 150000 ЗОЛЗАЯА АРИЛД',
                    'relatedAccount' => '2719323024',
                ],
            ],
        ];

        return $response['transactions'];
    }

    public function sendGetRequest($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $this->accessToken,
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }

    protected function findTnxId($textDescription)
    {
        $str = mb_strtoupper($textDescription, "UTF-8");
        $pieces = explode("ХААНААС:", $str);
        $str = $pieces[0];
        $index = mb_strpos($str, "TES", 0, "UTF-8");
        $length = mb_strlen($str);

        $i = $index + 3;
        $tnxId = "TES";

        do {
            $ch = mb_substr($str, $i, 1);
            if (is_numeric($ch)) {
                $tnxId .= $ch;
            }
            $i++;
        } while (is_numeric($ch) && $i < $length);

        return $tnxId;
    }

}
